Using the Layout Plugin Module allows to set different layouts for block wrapper theme.

This module affects only block.html.twig, and not the way content is displayed.

The layout selector is shown at the end of the block configuration form.
Installation

    Download and install Layout Plugin Module.
    Download and install Block Layout.
    Enter to the block configuration form and select layout.

For developers

New layouts can be defined using layout plugin wiith type=block. Detailed information on how to declare new layouts.