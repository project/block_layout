<?php

/**
 * @file
 * Contains \Drupal\block_layout\BlockLayoutStorage.
 */

namespace Drupal\block_layout;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Storage for bootstrap blocks entities.
 */
class BlockLayoutStorage extends ConfigEntityStorage {

}
